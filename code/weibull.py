import numpy as np

from scipy.special import gamma


def weibull(t, a, b):
    """The Weibull probility density function."""
    return (b / a) * (t / a)**(b - 1) * np.exp(-(t / a)**b)


def weibull_mean(a, b):
    return a * gamma(1 + 1 / b)


def weibull_quantile(a, b, p):
    return a * (-np.log(1 - p)) ** (1/b)
