import time

from keras.callbacks import TerminateOnNaN, ModelCheckpoint, CSVLogger 
from keras.layers import Dense, GRU, Lambda
from keras.models import Sequential
from keras.optimizers import adam
from keras import regularizers

import matplotlib.pyplot as plt
import numpy as np

from data_augmentation import clip_tte
from data_loading import get_path
from data_augmentation import load_and_augment_data
from training import generate_training_data, segment_timeseries_seq_to_seq, get_mean_error_seq_to_seq

def weibull_discrete_loss(y_true, y_pred):
    """
    Implementation of the discrete Weibull loss function.

    Adapted from an example by Martinsson.
    NOTE: OBSERVATIONS ARE NEVER CENSORED HERE!!!
    """
    # Import in function, since otherwise model loading doesn't work.
    import keras.backend as kb
    import tensorflow as tf

    # All the predictions are tensors containing for each time step both two values.
    # y, u = tf.unstack(y_true, num=2, axis=-1)
    a, b = tf.unstack(y_pred, num=2, axis=-1)
    y = y_true

    def hazard(y, a, b, plus_factor):
        """Cumulative hazard function for the Weibull distribution."""
        return kb.pow((y + plus_factor) / a, b)

    # Discrete, posibly censored Weibull log likelihood.
    # The factor 1e-35 is crucial, otherwise numerical errors compound and NaNs occur.
    # We only consider the non-censored observations.
    u = 1
    log_likelihood = u * kb.log(kb.exp(hazard(y, a, b, 1) - hazard(y, a, b, 1e-35)) - 1) - hazard(y, a, b, 1)

    # Since we wish to minimize the loss, we need to multiply by -1.
    loss = -1 * log_likelihood

    return loss


def weibull_discrete_loss_seq_to_seq(y_true, y_pred):
    """
    Implementation of the discrete Weibull loss function, closer to the Martinsson example, using seq2seq.

    Adapted from an example by Martinsson.
    NOTE: OBSERVATIONS ARE NEVER CENSORED HERE!!!
    """
    # Import in function, since otherwise model loading doesn't work.
    import keras.backend as kb
    import tensorflow as tf

    # All the predictions are tensors containing for each time step both two values.
    y, _ = tf.unstack(y_true, num=2, axis=-1)
    a, b = tf.unstack(y_pred, num=2, axis=-1)

    def hazard(y, a, b, plus_factor):
        """Cumulative hazard function for the Weibull distribution."""
        return kb.pow((y + plus_factor) / a, b)

    # Discrete, posibly censored Weibull log likelihood.
    # The factor 1e-35 is crucial, otherwise numerical errors compound and NaNs occur.
    # We only consider the non-censored observations.
    u = 1
    log_likelihood = u * kb.log(kb.exp(hazard(y, a, b, 1) - hazard(y, a, b, 1e-35)) - 1) - hazard(y, a, b, 1)

    # Since we wish to minimize the loss, we need to multiply by -1.
    loss = -1 * kb.mean(log_likelihood)

    return loss


def output_lambda(x, init_alpha=43.0, max_beta_value=5.0):
    """
    Computation of alpha and beta.

    Adapted from an example by Martinsson.
    """
    # Import in function, since otherwise model loading doesn't work.
    import keras.backend as kb
    import tensorflow as tf
    a, b = tf.unstack(x, num=2, axis=-1)

    # Implicitly initialize alpha:
    a = init_alpha * kb.exp(a)

    b = max_beta_value * kb.sigmoid(b)

    x = kb.stack([a, b], axis=-1)

    return x


def output_lambda_martinsson(x, init_alpha=43.0, max_beta_value=5.0, max_alpha_value=None):
    """Elementwise computation of alpha and regularized beta, as implemented by Martinsson."""
    import keras.backend as kb
    import tensorflow as tf
    a, b = tf.unstack(x, num=2, axis=-1)

    # Implicitly initialize alpha:
    if max_alpha_value is None:
        a = init_alpha * kb.exp(a)
    else:
        a = init_alpha * kb.clip(x=a, min_value=kb.epsilon(), max_value=max_alpha_value)

    if max_beta_value > 1.05:  # some value >>1.0
        # shift to start around 1.0
        # assuming input is around 0.0
        _shift = kb.log(max_beta_value - 1.0)

        b = kb.sigmoid(b - _shift)
    else:
        b = kb.sigmoid(b)

    # Clipped sigmoid : has zero gradient at 0,1
    # Reduces the small tendency of instability after long training
    # by zeroing gradient.
    b = max_beta_value * kb.clip(x=b, min_value=kb.epsilon(), max_value=1. - kb.epsilon())

    x = kb.stack([a, b], axis=-1)

    return x


def wtte_model(memory_length, init_alpha, features):
    model = Sequential()
    model.add(GRU(100, input_shape=(memory_length, len(features)), activation="tanh", return_sequences=True, dropout=0.2, recurrent_dropout=0.2, kernel_regularizer=regularizers.l1_l2(0.01, 0.01), recurrent_regularizer=regularizers.l1_l2(0.01, 0.01), bias_regularizer=regularizers.l1_l2(0.01, 0.01)))
    model.add(GRU(50, input_shape=(memory_length, 1), activation="tanh", return_sequences=True, dropout=0.2, recurrent_dropout=0.2))
    model.add(Dense(50, kernel_regularizer=regularizers.l1_l2(0.01, 0.01), bias_regularizer=regularizers.l1_l2(0.01, 0.01)))
    model.add(Dense(2, kernel_regularizer=regularizers.l1_l2(0.01, 0.01), bias_regularizer=regularizers.l1_l2(0.01, 0.01)))
    model.add(Lambda(output_lambda_martinsson, arguments={"init_alpha": init_alpha, "max_beta_value": 10.0}))

    model.compile(loss=weibull_discrete_loss_seq_to_seq, optimizer=adam(lr=0.001))

    model.summary()

    return model

def wtte_seq2seq_meta(clip_length,
                      cutoff,
                      drop_censored,
                      drop_criteria_column,
                      features,
                      jam_ratio,
                      memory_length,
                      min_jam_length,
                      min_non_jam_length,
                      normalize_x,
                      use_log,
                      y_train_variable,
                      show_plots=False,
                      epochs=35):
    """Meta function that runs a given calculatation of WTTE."""
    model_type = "wtte_seq2seq"

    model_name = f"{model_type}_{clip_length}_{cutoff}_{drop_censored}_{drop_criteria_column}_{'-'.join(features)}_{str(jam_ratio).replace('.', '')}_{min_jam_length}_{min_non_jam_length}_{memory_length}_{normalize_x}_{use_log}_{y_train_variable}"
    print(model_name)

    df = load_and_augment_data(get_path(),
                               min_non_jam_length=min_non_jam_length,
                               min_jam_length=min_jam_length,
                               jam_ratio=jam_ratio,
                               show_final_statistics=False,
                               show_plots=show_plots)

    df = clip_tte(df, clip_length=clip_length, use_log=use_log, show_plots=show_plots)

    (x_train, y_train,
     x_val, y_val,
     x_test, y_test) = generate_training_data(df, drop_censored=drop_censored,
                                              drop_criteria_column=drop_criteria_column,
                                              cutoff=cutoff,
                                              show_plots=False,
                                              y_train_variable=y_train_variable,
                                              features=features,
                                              normalize=normalize_x)

    x_train_segmented, y_train_segmented = segment_timeseries_seq_to_seq(x_train, y_train, length=memory_length)
    x_val_segmented, y_val_segmented = segment_timeseries_seq_to_seq(x_val, y_val, length=memory_length)
    x_test_segmented, y_test_segmented = segment_timeseries_seq_to_seq(x_test, y_test, length=memory_length)

    x_train_segmented = x_train_segmented.reshape(-1, memory_length, len(features))
    y_train_segmented = y_train_segmented.reshape(-1, memory_length, 1)

    x_val_segmented = x_val_segmented.reshape(-1, memory_length, len(features))
    y_val_segmented = y_val_segmented.reshape(-1, memory_length, 1)

    print(x_train_segmented.shape)
    print(y_train_segmented.shape)

    print(x_val_segmented.shape)
    print(y_val_segmented.shape)

    print(x_test_segmented.shape)
    print(y_test_segmented.shape)

    # Seq2seq doesn't work without something acting as a (dummy) censoring indicator.
    y_train_segmented2 = np.repeat(y_train_segmented, 2, axis=-1)
    y_val_segmented2 = np.repeat(y_val_segmented, 2, axis=-1)
    y_test_segmented2 = np.repeat(y_test_segmented, 2, axis=-1)

    print(y_train_segmented2.shape)
    print(y_val_segmented2.shape)
    print(y_test_segmented2.shape)

    init_alpha = -1.0/np.log(1.0-1.0/(np.mean(y_train_segmented)+1.0))
    print("init_alpha:", init_alpha)

    np.random.seed(1)

    model = wtte_model(memory_length, init_alpha, features)

    current_time = time.time()

    history = model.fit(x_train_segmented, y_train_segmented2.astype(np.float32),
                        epochs=epochs,
                        batch_size=2325,
                        verbose=1,
                        callbacks=[
                            TerminateOnNaN(),
                            ModelCheckpoint(f"D:/models/{model_name}" + "_{epoch:02d}_{val_loss}_" + f"{current_time:0.0f}.hdf5", save_best_only=True, monitor="val_loss", verbose=1),
                            CSVLogger(f"D:/models/{model_name}" + "_{epoch:02d}_{val_loss}_" + f"{current_time:0.0f}.csv")
                        ],
                        validation_data=(x_val_segmented,
                                         y_val_segmented2))

    model.save(f"D:/models/{model_name}_last-epoch_{current_time:0.0f}.hdf5")

    plt.plot(history.history["loss"], label="training")
    plt.plot(history.history["val_loss"], label="validation")
    plt.xlabel("epoch")
    plt.ylabel("loss")
    plt.legend()
    plt.show()

    return get_mean_error_seq_to_seq(model=model, x_test=x_test_segmented, y_test=y_test_segmented, memory_length=memory_length, reverse_log=use_log, len_features=len(features))
