from keras.models import load_model

import matplotlib.pyplot as plt

import numpy as np

from sklearn.metrics import mean_squared_error, mean_absolute_error, log_loss

from plotting import plot_ttes

from weibull import weibull_mean, weibull_quantile

# from wtte import weibull_discrete_loss


def plot_results(y_test, pred, x, i, reverse_log, mse_error_unlogged, mae_error_unlogged, ce_unlogged):
    if reverse_log:
        print(f"MSE ('unlogged', whole test set): {mse_error_unlogged}")
        print(f"MAE ('unlogged', whole test set): {mae_error_unlogged}")
        print(f"CE ('unlogged', whole test set): {ce_unlogged}")
        # Plot on a few different zoom scales.
        plot_ttes(y_test[i].ravel(), pred, max_y=np.max(np.expm1(y_test[i])) + 1, x_limit_range=(0, len(x)), reverse_log=reverse_log)
        plot_ttes(y_test[i].ravel(), pred, max_y=np.max(np.expm1(y_test[i])) + 1, x_limit_range=(3000, 4000), reverse_log=reverse_log)
    else:
        # Plot on a few different zoom scales.
        plot_ttes(y_test[i].ravel(), pred, max_y=np.max(y_test[i]) + 1, x_limit_range=(0, len(x)), reverse_log=reverse_log)
        plot_ttes(y_test[i].ravel(), pred, max_y=np.max(y_test[i]) + 1, x_limit_range=(3000, 4000), reverse_log=reverse_log)
        


def get_ce_loss(y_true, y_pred):
    """Calculate the log loss error (cross entropy) from a prediction for tte."""
    clipped_true = np.clip(y_true, a_min=0, a_max=1).ravel()
    clipped_pred = np.floor(np.clip(y_pred, a_min=0, a_max=1)).astype(int)

    return log_loss(clipped_true, clipped_pred, labels=[0, 1])


def get_mean_error_seq_to_seq(model, x_test, y_test, memory_length, reverse_log, len_features, show_plots=True):
    mse = []
    mse_unlogged = []
    mae = []
    mae_unlogged = []
    ce = []
    ce_unlogged = []
    for i, road in enumerate(x_test):
        print(f"Test road {i}/{len(x_test)}")
        pred = model.predict(road, batch_size=2000)
        pred = weibull_quantile(pred[:, :, 0], pred[:, :, 1], 0.6)

        mse_error = mean_squared_error(y_test[i].ravel(), pred.ravel())
        # mse_error_unlogged = mean_squared_error(np.expm1(y_test[i].ravel()), np.expm1(pred.reshape(-1, 1)))
        mae_error = mean_absolute_error(y_test[i].ravel(), pred.ravel())
        # mae_error_unlogged = mean_absolute_error(np.expm1(y_test[i].ravel()), np.expm1(pred.reshape(-1, 1)))
        ce_error = get_ce_loss(y_test[i], pred.ravel())
        # ce_error_unlogged = get_ce_loss(np.expm1(y_test[i].ravel()), np.expm1(pred.ravel()))
        mse.append(mse_error)
        # mse_unlogged.append(mae_error_unlogged)
        mae.append(mae_error)
        # mae_unlogged.append(mae_error_unlogged)
        ce.append(ce_error)
        # ce_unlogged.append(ce_error_unlogged)

        if show_plots:
            print(f"MSE: {mse_error}")
            print(f"MAE: {mae_error}")
            print(f"CE: {ce_error}")
            plot_results(y_test, pred.ravel(), pred.ravel(), i, reverse_log, 0, 0, 0)

    print(f"MSE (whole test set): {np.mean(np.array(mse))}")
    print(f"MAE (whole test set): {np.mean(np.array(mae))}")
    print(f"CE (whole test set): {np.mean(np.array(ce))}")
    if reverse_log:
        print(f"MSE ('unlogged', whole test set): {np.mean(np.array(mse_unlogged))}")
        print(f"MAE ('unlogged', whole test set): {np.mean(np.array(mae_unlogged))}")
        print(f"CE ('unlogged', whole test set): {np.mean(np.array(ce_unlogged))}")
        return np.mean(np.array(mse_unlogged)), np.mean(np.array(mae_unlogged)), np.mean(np.array(ce_unlogged))
    else:
        return np.mean(np.array(mse)), np.mean(np.array(mae)), np.mean(np.array(ce))


def get_mean_error(model, x_test, y_test, memory_length, reverse_log, len_features, model_type, show_plots=True, cutoff=30000):
    mse = []
    mse_unlogged = []
    mae = []
    mae_unlogged = []
    ce = []
    ce_unlogged = []
    for i, x in enumerate(x_test):
        print(f"Test road {i}/{len(x_test)}")
        if model_type == "rf":
            pred = model.predict(x.reshape(-1, memory_length * len_features))
        elif model_type == "lstm":
            pred = model.predict(x, batch_size=1000)
        elif model_type == "wtte":
            pred = model.predict(x, batch_size=1000)
            pred = weibull_quantile(pred[:, 0, 0], pred[:, 0, 1], 0.6)
        elif model_type == "wtte-naive":
            pred = model.predict(x)
        # mse_error = mean_squared_error(y_test[i].reshape(cutoff), pred.reshape(cutoff))
        mse_error = mean_squared_error(y_test[i], pred)

        mse_error_unlogged = 0  # mean_squared_error(np.expm1(y_test[i]), np.expm1(pred.reshape(-1, 1)))
        # mae_error = mean_absolute_error(y_test[i].reshape(cutoff), pred.reshape(cutoff))
        mae_error = mean_absolute_error(y_test[i], pred)

        mae_error_unlogged = 0  # mean_absolute_error(np.expm1(y_test[i]), np.expm1(pred.reshape(-1, 1)))
        # ce_error = get_ce_loss(y_test[i], pred.reshape(cutoff))
        ce_error = get_ce_loss(y_test[i], pred)
        ce_error_unlogged = 0  # get_ce_loss(np.expm1(y_test[i]), np.expm1(pred.ravel()))
        mse.append(mse_error)
        mse_unlogged.append(mae_error_unlogged)
        mae.append(mae_error)
        mae_unlogged.append(mae_error_unlogged)
        ce.append(ce_error)
        ce_unlogged.append(ce_error_unlogged)

        if show_plots:
            print(f"MSE: {mse_error}")
            print(f"MAE: {mae_error}")
            print(f"CE: {ce_error}")
            plot_results(y_test, pred, x, i, reverse_log, mse_error_unlogged, mae_error_unlogged, ce_error_unlogged)

    print(f"MSE (whole test set): {np.mean(np.array(mse))}")
    print(f"MAE (whole test set): {np.mean(np.array(mae))}")
    print(f"CE (whole test set): {np.mean(np.array(ce))}")
    if reverse_log:
        print(f"MSE ('unlogged', whole test set): {np.mean(np.array(mse_unlogged))}")
        print(f"MAE ('unlogged', whole test set): {np.mean(np.array(mae_unlogged))}")
        print(f"CE ('unlogged', whole test set): {np.mean(np.array(ce_unlogged))}")
        return np.mean(np.array(mse_unlogged)), np.mean(np.array(mae_unlogged)), np.mean(np.array(ce_unlogged))
    else:
        return np.mean(np.array(mse)), np.mean(np.array(mae)), np.mean(np.array(ce))


def segment_timeseries(x_data, y_data, length):
    x_segmented = []
    y_segmented = []
    for i in range(len(x_data)):
        sub_x_segmented = []
        sub_y_segmented = []
        for j in range(len(x_data[i]) - length):
            sub_x_segmented.append(x_data[i][j:j+length])
            sub_y_segmented.append(y_data[i][j+length])
        x_segmented.append(sub_x_segmented)
        y_segmented.append(sub_y_segmented)

    x_segmented = np.array(x_segmented)
    y_segmented = np.array(y_segmented)

    print(f"x shape: {x_segmented.shape}, y shape: {y_segmented.shape}")

    return x_segmented, y_segmented


def segment_timeseries_seq_to_seq(x_data, y_data, length):
    """No overlap between sequences"""
    x_segmented = []
    y_segmented = []
    for i in range(len(x_data)):
        sub_x_segmented = []
        sub_y_segmented = []
        for j in range(0, len(x_data[i]), length):
            sub_x_segmented.append(x_data[i][j:j+length])
            sub_y_segmented.append(y_data[i][j:j+length])
        x_segmented.append(sub_x_segmented)
        y_segmented.append(sub_y_segmented)

    x_segmented = np.array(x_segmented)
    y_segmented = np.array(y_segmented)

    print(f"x shape: {x_segmented.shape}, y shape: {y_segmented.shape}")

    return x_segmented, y_segmented


def prepare_individual_df(features, df, y_train_variable, cutoff, normalize):
    """
    normalize: normalize training data input using hyperbolic tangent (Reals -> [-1, 1])
    """
    ratio_array = df.groupby("measurementSiteReference")["ratioReference"].apply(lambda x: np.array(x)[:cutoff]).to_numpy()
    tod_array = df.groupby("measurementSiteReference")["timeOfDay"].apply(lambda x: np.array(x)[:cutoff]).to_numpy()
    speed_array = df.groupby("measurementSiteReference")["avgVehicleSpeed"].apply(lambda x: np.array(x)[:cutoff]).to_numpy()

    ratio_array = np.hstack(ratio_array.flat).reshape(len(ratio_array), cutoff, -1)
    tod_array = np.hstack(tod_array.flat).reshape(len(tod_array), cutoff, -1)
    speed_array = np.hstack(speed_array.flat).reshape(len(speed_array), cutoff, -1)

    if normalize:
        ratio_array = np.tanh(ratio_array - 1)

    feature_map = {"ratioReference": ratio_array, "timeOfDay": tod_array, "avgVehicleSpeed": speed_array}

    to_stack = [feature_map[x] for x in features]

    x_data = np.stack(to_stack, axis=-1)

    y_data = df.groupby("measurementSiteReference")[y_train_variable].apply(lambda x: np.array(x)[:cutoff]).to_numpy()
    y_data = np.hstack(y_data.flat).reshape(len(y_data), cutoff, -1)

    return x_data, y_data


def prepare_data(features, df_train, df_val, df_test, y_train_variable, normalize, cutoff=30000):
    """
    Prepare data for training.

    features: features to be used
    cutoff: length at which to truncate
    """
    x_train, y_train = prepare_individual_df(features, df_train, y_train_variable, cutoff, normalize=normalize)
    x_train = x_train.reshape((len(x_train), cutoff, len(features)))
    print(f"x_train: {x_train.shape}, y_train: {y_train.shape}")

    # Validation set is not used in random forest.
    if df_val is not None:
        x_val, y_val = prepare_individual_df(features, df_val, y_train_variable, cutoff, normalize=normalize)
        x_val = x_val.reshape((len(x_val), cutoff, len(features)))
        print(f"x_val: {x_val.shape}, y_val: {y_val.shape}")
    else:
        x_val = None
        y_val = None

    x_test, y_test = prepare_individual_df(features, df_test, y_train_variable, cutoff, normalize=normalize)
    x_test = x_test.reshape((len(x_test), cutoff, len(features)))
    print(f"x_test: {x_test.shape}, y_test: {y_test.shape}")

    return x_train, y_train, x_val, y_val, x_test, y_test


def drop_short_sequences(df, cutoff, show_plots=True):
    """Drop road sequences that have fewer uncensored data points than the cutoff."""
    if show_plots:
        df.groupby("measurementSiteReference").apply(lambda x: len(x)).hist(bins=20)
        plt.axvline(x=30000, color="red", label="cutoff")
        plt.legend()
        plt.xlabel("length of sequence")
        plt.ylabel("number of sequences")
        plt.show()

    # Drop sequences shorter than the cutoff.
    df = df.groupby("measurementSiteReference").filter(lambda x: len(x) > cutoff).reset_index(level=0, drop=True)

    return df


def generate_train_test_split(df, drop_censored, drop_criteria_column):
    """
    drop_criteria_column: which column to consider when dropping censored data.
    """
    roads = sorted(df["measurementSiteReference"].unique())

    validation_roads = roads[0::15]
    testing_roads = roads[1::15]

    # We need to make sure we exclude the roads of the validation and test sets in the training set.
    non_training_indices = sorted(list(range(0, len(roads), 15)) + list(range(1, len(roads), 15)))
    training_indices = [i for i in range(0, len(roads)) if i not in non_training_indices]
    training_roads = [roads[i] for i in training_indices]

    df_train = df[df["measurementSiteReference"].isin(training_roads)]
    df_val = df[df["measurementSiteReference"].isin(validation_roads)]
    df_test = df[df["measurementSiteReference"].isin(testing_roads)]

    # For basic regression we cannot use censored observations.
    if drop_censored:
        df_train = df_train[df_train[drop_criteria_column] == 1]
        df_val = df_val[df_val[drop_criteria_column] == 1]
        df_test = df_test[df_test[drop_criteria_column] == 1]

    return df_train, df_val, df_test


def generate_training_data(df, drop_censored, drop_criteria_column, cutoff, show_plots, y_train_variable, features, normalize):
    # This normalization happens here, since ithas to be the same for all data.
    if normalize:
        df["avgVehicleSpeed"] = np.tanh((df["avgVehicleSpeed"] - df["avgVehicleSpeed"].mean())/df["avgVehicleSpeed"].std())

    df_train, df_val, df_test = generate_train_test_split(df, drop_censored=drop_censored, drop_criteria_column=drop_criteria_column)

    df_train = drop_short_sequences(df_train, cutoff=cutoff, show_plots=show_plots)
    df_val = drop_short_sequences(df_val, cutoff=cutoff, show_plots=show_plots)
    df_test = drop_short_sequences(df_test, cutoff=cutoff, show_plots=show_plots)

    x_train, y_train, x_val, y_val, x_test, y_test = prepare_data(features, df_train, df_val, df_test, y_train_variable=y_train_variable, normalize=normalize)

    return x_train, y_train, x_val, y_val, x_test, y_test


def drop_overabundant_observations(x_train, y_train, clip_length, sequence_length, no_of_features):
    """Due to severe class imbalance, we drop all 0, and 'clip_length' observations."""
    y_ravelled = y_train.ravel()
    useful_sample_indices = np.where((y_ravelled > 0) & (y_ravelled < clip_length))[0]
    useful_y_data = y_ravelled[useful_sample_indices].reshape(-1, 1)

    x_ravelled = x_train.reshape(-1, sequence_length, no_of_features)
    useful_x_data = x_ravelled[useful_sample_indices]

    return useful_x_data, useful_y_data


# def load_wtte_model(path):
#     return load_model(path, custom_objects={"weibull_discrete_loss": weibull_discrete_loss})
