import glob
import platform
import pprint
import time

import numpy as np
import pandas as pd


def get_path():
    if platform.system() == "Darwin":
        return "/Users/Teun/Downloads/Data/Alle FCD PZH juli 2017 zonder meta_20170726T090051_407"
    elif platform.system() == "Windows":
        return "C:/Users/Gebruiker/Desktop/Data/Alle FCD PZH juli 2017 zonder meta_20170726T090051_407"
    else:
        raise "Platform not supported."


def shape_of(df):
    print(f"Data frame has shape {df.shape}")


def get_no_of_unique_values(df):
    """Show the number of unique values in each column."""
    column_unique_values = {}
    for i in df.columns:
        column_unique_values[i] = len(df[i].unique())

    pprint.pprint(column_unique_values)


def load_FCD(path):
    """Load FCD data from path."""
    start_time = time.time()

    all_files = glob.glob(path + "/*.csv")

    # Concatenating method from https://stackoverflow.com/a/36416258
    partial_dfs = []
    for f in sorted(all_files):
        partial_df = pd.read_csv(f, header=0,
                                 usecols=["measurementSiteReference",
                                          "periodStart",
                                          "dataError",
                                          "avgVehicleSpeed",
                                          "avgTravelTime",
                                          "lengthAffected",
                                          "referenceAvgVehicleSpeed",
                                          "referenceAvgTravelTime"],
                                 parse_dates=["periodStart"])
        partial_dfs.append(partial_df)

    df = pd.concat(partial_dfs, axis=0, ignore_index=True)

    # Replace invalid travel times with NaNs.
    df["avgTravelTime"] = df["avgTravelTime"].where(df["avgTravelTime"] > 0, np.nan)

    # Change NaNs in dataError column into 0s, and convert column to indicator.
    df["dataError"] = df["dataError"].transform(lambda x: x.fillna(0))
    df["dataError"] = df["dataError"].astype(int)
    
    # Add time of day indicator column.
    df["timeOfDay"] = np.sin(2 * np.pi * (df["periodStart"].dt.hour * 60 + df["periodStart"].dt.minute) / 1440)

    print(f"Loading took {time.time() - start_time:.2f} seconds")

    shape_of(df)

    return df


def drop_columns(df, columns_to_drop):
    """Useful for keeping memory usage in check"""
    print(f"Dropping columns: {columns_to_drop}")
    df.drop(columns_to_drop, axis="columns", errors="ignore", inplace=True)
