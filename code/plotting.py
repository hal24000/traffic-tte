import datetime

try:
    import contextily as ctx
except ImportError:
    pass

try:
    import geopandas as gpd
except ImportError:
    pass

from matplotlib.colors import LogNorm
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter

import numpy as np

import pandas as pd

from scipy import stats

import seaborn as sns

from statsmodels.graphics import tsaplots

from weibull import weibull


def plot_tte_for_report(true, prediction, max_y, x_limit_range, savefile=None):
    fig, ax = plt.subplots()

    print(f"Start date: {datetime.datetime(2017, 7, 1, 0, 0, 0) + datetime.timedelta(minutes=x_limit_range[0])}")
    dates = [datetime.datetime(2017, 7, 1, 0, 0, 0) + datetime.timedelta(minutes=x_limit_range[0]) + datetime.timedelta(minutes=x) for x in range(0, x_limit_range[1]-x_limit_range[0])]
    ax.plot(dates, prediction.ravel()[x_limit_range[0]:x_limit_range[1]], label="predicted TTE")
    ax.plot(dates, true[x_limit_range[0]:x_limit_range[1]], color="red", label="true TTE")

    ax.xaxis.set_major_formatter(DateFormatter("%H:%M"))

    plt.xlabel("time of day")
    plt.ylabel("time-to-event (minutes)")
    plt.xlim((datetime.datetime(2017, 7, 1, 0, 0, 0) + datetime.timedelta(minutes=x_limit_range[0]), datetime.datetime(2017, 7, 1, 0, 0, 0) + datetime.timedelta(minutes=x_limit_range[1])))
    plt.ylim((0, max_y + 1))
    plt.legend()
    plt.tight_layout()
    if savefile:
        plt.savefig(savefile)
    plt.show()


def plot_over_day(quantities, labels, y_label="number of measurements", save_location=None):
    assert (len(quantities) == len(labels)), "Wrong number of quantities to plot and labels."

    fig, ax = plt.subplots()
    fig.autofmt_xdate()
    ax.xaxis.set_major_locator(mdates.HourLocator(interval=3))
    ax.xaxis.set_major_formatter(mdates.DateFormatter("%H:%M"))

    for k in range(len(quantities)):
        plt.plot(pd.date_range(start="01-01-2019", periods=1440, freq="min"), quantities[k], label=labels[k])

    plt.xlabel("time of day")
    plt.ylabel(y_label)

    ax.set_xlim([datetime.datetime(2019, 1, 1, 0, 0), datetime.datetime(2019, 1, 2, 0, 0)])

    plt.legend()

    plt.tight_layout()

    if save_location:
        plt.savefig(save_location)

    plt.show()


def plot_time_to_event(df, road):
    partial_df = df[df["measurementSiteReference"] == road].reset_index(drop=True)    
    partial_df = partial_df[partial_df["timeToJamUncensored"] == 1]

    fig, ax = plt.subplots()
    fig.autofmt_xdate()

    partial_df.plot(x="periodStart", y="timeToJam", legend=None, ax=ax)
    plt.ylim(bottom=0)
    plt.xlabel("July 2017")
    plt.ylabel("time-to-event (minutes)")

    ax.xaxis.set_major_locator(mdates.WeekdayLocator(interval=1))
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%d'))

    plt.tight_layout()
    plt.savefig("../thesis/figures/ttejams.pdf")
    plt.show()


def plot_dist_vs_tte(prediction, true, clip_length, memory_length):
    distribution = np.array([weibull(np.arange(0, clip_length + 1, 1), p[0], p[1]) for p in prediction.reshape((-1, 2))])
    plt.plot(np.argmax(distribution, axis=1))
    plt.show()
    for k in range(0, 30000, memory_length):
        plt.imshow(distribution[k:k+memory_length].T, origin="lower", aspect=5)
        plt.plot(true[k:k+memory_length], color="black")
        plt.ylim(0, clip_length)
        plt.ylabel("time-to-event")
        plt.show()


def plot_speed_over_day(df):
    plot_over_day(
        [df.groupby([df.periodStart.dt.hour, df.periodStart.dt.minute])["avgVehicleSpeed"].quantile(0.75),
         df.groupby([df.periodStart.dt.hour, df.periodStart.dt.minute])["avgVehicleSpeed"].median(),
         df.groupby([df.periodStart.dt.hour, df.periodStart.dt.minute])["avgVehicleSpeed"].quantile(0.25)
        ],
        labels=["75th quantile", "mean", "25th quantile"],
        y_label="speed (m/s)"
    )


def true_predict_scatter_plot(predictions, true_values, max_value=30):
    j = sns.JointGrid(x=predictions, y=true_values.ravel(), xlim=(0, max_value), ylim=(0, max_value))
    j = j.plot_joint(plt.scatter)
    j = j.plot_marginals(sns.distplot, kde=True)
    j = j.annotate(stats.pearsonr)
    j.ax_joint.set_xlabel("prediction")
    j.ax_joint.set_ylabel("true value")
    plt.plot()


def lognorm_scatter_plot(predictions, true_values, max_values):
    plt.hist2d(x=predictions, y=true_values.ravel(), norm=LogNorm(), bins=(30, 30))
    plt.xlabel("prediction")
    plt.ylabel("true value")
    plt.show()


def plot_ttes(true, prediction, max_y, x_limit_range, reverse_log=False):
    """reverse_log: if True, exponentiate values for plotting, to undo clipping through taking the logarithm."""
    if reverse_log:
        plt.plot(np.expm1(prediction.ravel()), label="predicted TTE")
        plt.plot(np.expm1(true), color="red", label="true TTE")

    else:
        plt.plot(prediction.ravel(), label="predicted TTE")
        plt.plot(true, color="red", label="true TTE")

    plt.xlabel("time (minutes)")
    plt.ylabel("time-to-event (minutes)")
    plt.xlim(x_limit_range)
    plt.ylim((0, max_y + 1))
    plt.legend()
    plt.show()


def plot_pacf(df, road, variable, lags=20):
    tsaplots.plot_pacf(df[df["measurementSiteReference"] == road][variable], lags=lags)
    plt.show()


def plot_distribution_of_data(df_train, df_val, df_test):
    for x in [df_train, df_val, df_test]:
        print(x["isJam"].value_counts(sort=False) / len(x["isJam"]))
        print(x["timeToJamClipped"].value_counts(sort=False) / len(x["timeToJamClipped"]))

        plt.semilogy(x["timeToJamClipped"].value_counts(sort=False).to_numpy() / len(x["timeToJamClipped"]))

        print("\n")

    plt.show()


def plot_weibull():
    vweibull = np.vectorize(weibull)
    plt.plot(np.linspace(0, 2, 100), vweibull(np.linspace(0, 2, 100), 1, 1), label=r"$\alpha=1$, $\beta=1$")
    plt.plot(np.linspace(0, 2, 100), vweibull(np.linspace(0, 2, 100), 1, 1.5), label=r"$\alpha=1$, $\beta=1.5$")
    plt.plot(np.linspace(0, 2, 100), vweibull(np.linspace(0, 2, 100), 1, 5), label=r"$\alpha=1$, $\beta=5$")

    plt.xlabel(r"$t$")
    plt.ylabel("PDF")
    plt.legend()
    plt.xlim(0, 2)
    plt.tight_layout()
    plt.savefig("../thesis/figures/weibull_pdf.pdf")


def plot_training_data(x_train, y_train):
    plt.imshow(x_train[0:100, :2000, :].sum(axis=2), aspect="auto")
    plt.xlabel("time step")
    plt.ylabel("training road index")
    plt.title("x training data")
    plt.colorbar()
    plt.show()

    if y_train is not None:
        plt.imshow(y_train[:100, :2000, 0], aspect="auto")
        plt.xlabel("time step")
        plt.ylabel("training road index")
        plt.title("y training data")
        plt.colorbar()
        plt.show()


def plot_roads():
    """We hardcode the roads to avoid having to load in the data."""

    roads = ['PZH03_N206-1_a',
             'PZH03_N206-1_a-1',
             'PZH03_N206-1_b',
             'PZH03_N206-1_b-1',
             'PZH03_N206-1_c',
             'PZH03_N206-1_c-1',
             'PZH03_N206-1_d',
             'PZH03_N206-1_d-1',
             'PZH03_N206-1_e',
             'PZH03_N206-1_e-1',
             'PZH03_N206-1_f',
             'PZH03_N206-1_f-1',
             'PZH03_N206-1_g',
             'PZH03_N206-1_g-1',
             'PZH03_N206-1_h',
             'PZH03_N206-1_h-1',
             'PZH03_N206-2_a',
             'PZH03_N206-2_a-1',
             'PZH03_N206-2_b',
             'PZH03_N206-2_b-1',
             'PZH03_N206-2_c',
             'PZH03_N206-2_c-1',
             'PZH03_N206-2_d',
             'PZH03_N206-2_d-1',
             'PZH03_N206-2_e',
             'PZH03_N206-2_e-1',
             'PZH03_N206-2_f',
             'PZH03_N206-2_f-1',
             'PZH03_N206-2_g',
             'PZH03_N206-2_g-1',
             'PZH03_N206-2_h',
             'PZH03_N206-2_h-1',
             'PZH03_N206-2_i',
             'PZH03_N206-2_i-1',
             'PZH03_N206-3_a',
             'PZH03_N206-3_a-1',
             'PZH03_N206-3_b',
             'PZH03_N206-3_b-1',
             'PZH03_N206-3_c',
             'PZH03_N206-3_c-1',
             'PZH03_N206-3_d',
             'PZH03_N206-3_d-1',
             'PZH03_N206-3_e',
             'PZH03_N206-3_e-1',
             'PZH03_N206-3_f',
             'PZH03_N206-3_f-1',
             'PZH03_N206-3_g',
             'PZH03_N206-3_g-1',
             'PZH03_N206-3_h',
             'PZH03_N206-3_h-1',
             'PZH03_N206-3_i',
             'PZH03_N206-3_i-1',
             'PZH03_N207-1_a',
             'PZH03_N207-1_a-1',
             'PZH03_N207-1_b',
             'PZH03_N207-1_b-1',
             'PZH03_N207-1_c',
             'PZH03_N207-1_c-1',
             'PZH03_N207-1_d',
             'PZH03_N207-1_d-1',
             'PZH03_N207-1_e',
             'PZH03_N207-1_e-1',
             'PZH03_N207-1_f',
             'PZH03_N207-1_f-1',
             'PZH03_N207-1_g',
             'PZH03_N207-1_g-1',
             'PZH03_N207-1_h',
             'PZH03_N207-1_h-1',
             'PZH03_N207-1_i',
             'PZH03_N207-1_i-1',
             'PZH03_N207-2_a',
             'PZH03_N207-2_a-1',
             'PZH03_N207-2_b',
             'PZH03_N207-2_b-1',
             'PZH03_N207-2_c',
             'PZH03_N207-2_c-1',
             'PZH03_N207-2_d',
             'PZH03_N207-2_d-1',
             'PZH03_N207-2_e',
             'PZH03_N207-2_e-1',
             'PZH03_N207-2_f',
             'PZH03_N207-2_f-1',
             'PZH03_N207-2_g',
             'PZH03_N207-2_g-1',
             'PZH03_N207-2_h',
             'PZH03_N207-2_h-1',
             'PZH03_N207-2_i',
             'PZH03_N207-2_i-1',
             'PZH03_N207-3_a',
             'PZH03_N207-3_a-1',
             'PZH03_N207-3_b',
             'PZH03_N207-3_b-1',
             'PZH03_N207-3_c',
             'PZH03_N207-3_c-1',
             'PZH03_N207-3_d',
             'PZH03_N207-3_d-1',
             'PZH03_N207-3_e',
             'PZH03_N207-3_e-1',
             'PZH03_N207-3_f',
             'PZH03_N207-3_f-1',
             'PZH03_N207-3_g',
             'PZH03_N207-3_g-1',
             'PZH03_N207-3_h',
             'PZH03_N207-3_h-1',
             'PZH03_N207-3_i',
             'PZH03_N207-3_i-1',
             'PZH03_N207-4_a',
             'PZH03_N207-4_a-1',
             'PZH03_N207-4_b',
             'PZH03_N207-4_b-1',
             'PZH03_N208_a',
             'PZH03_N208_a-1',
             'PZH03_N208_b',
             'PZH03_N208_b-1',
             'PZH03_N208_c',
             'PZH03_N208_c-1',
             'PZH03_N208_d',
             'PZH03_N208_d-1',
             'PZH03_N208_e',
             'PZH03_N208_e-1',
             'PZH03_N208_f',
             'PZH03_N208_f-1',
             'PZH03_N208_g',
             'PZH03_N208_g-1',
             'PZH03_N209-1_a',
             'PZH03_N209-1_a-1',
             'PZH03_N209-1_b',
             'PZH03_N209-1_b-1',
             'PZH03_N209-1_c',
             'PZH03_N209-1_c-1',
             'PZH03_N209-1_d',
             'PZH03_N209-1_d-1',
             'PZH03_N209-1_e',
             'PZH03_N209-1_e-1',
             'PZH03_N209-1_f',
             'PZH03_N209-1_f-1',
             'PZH03_N209-1_g',
             'PZH03_N209-1_g-1',
             'PZH03_N209-1_h',
             'PZH03_N209-1_h-1',
             'PZH03_N209-1_i',
             'PZH03_N209-1_i-1',
             'PZH03_N209-1_k',
             'PZH03_N209-1_k-1',
             'PZH03_N209-2_a',
             'PZH03_N209-2_a-1',
             'PZH03_N209-2_b',
             'PZH03_N209-2_b-1',
             'PZH03_N209-2_c',
             'PZH03_N209-2_c-1',
             'PZH03_N209-2_d',
             'PZH03_N209-2_d-1',
             'PZH03_N209-2_e',
             'PZH03_N209-2_e-1',
             'PZH03_N209-2_f',
             'PZH03_N209-2_f-1',
             'PZH03_N209-2_g',
             'PZH03_N209-2_g-1',
             'PZH03_N209-2_h',
             'PZH03_N209-2_h-1',
             'PZH03_N209-2_i',
             'PZH03_N209-2_i-1',
             'PZH03_N210-1_a',
             'PZH03_N210-1_a-1',
             'PZH03_N210-1_b',
             'PZH03_N210-1_b-1',
             'PZH03_N210-1_c',
             'PZH03_N210-1_c-1',
             'PZH03_N210-1_d',
             'PZH03_N210-1_d-1',
             'PZH03_N210-1_e',
             'PZH03_N210-1_e-1',
             'PZH03_N210-1_f',
             'PZH03_N210-1_f-1',
             'PZH03_N210-1_g',
             'PZH03_N210-1_g-1',
             'PZH03_N210-1_h',
             'PZH03_N210-1_h-1',
             'PZH03_N210-2_a',
             'PZH03_N210-2_a-1',
             'PZH03_N210-2_b',
             'PZH03_N210-2_b-1',
             'PZH03_N210-2_c',
             'PZH03_N210-2_c-1',
             'PZH03_N211-1_a',
             'PZH03_N211-1_a-1',
             'PZH03_N211-1_b',
             'PZH03_N211-1_b-1',
             'PZH03_N211-1_c',
             'PZH03_N211-1_c-1',
             'PZH03_N211-1_d',
             'PZH03_N211-1_d-1',
             'PZH03_N211-1_e',
             'PZH03_N211-1_e-1',
             'PZH03_N211-1_f',
             'PZH03_N211-1_f-1',
             'PZH03_N211-1_g',
             'PZH03_N211-1_g-1',
             'PZH03_N211-1_h',
             'PZH03_N211-1_h-1',
             'PZH03_N211-2_a',
             'PZH03_N211-2_a-1',
             'PZH03_N211-2_b',
             'PZH03_N211-2_b-1',
             'PZH03_N211-2_c',
             'PZH03_N211-2_c-1',
             'PZH03_N211-2_d',
             'PZH03_N211-2_d-1',
             'PZH03_N211-2_e',
             'PZH03_N211-2_e-1',
             'PZH03_N213_a',
             'PZH03_N213_a-1',
             'PZH03_N213_b',
             'PZH03_N213_b-1',
             'PZH03_N213_c',
             'PZH03_N213_c-1',
             'PZH03_N213_d',
             'PZH03_N213_d-1',
             'PZH03_N213_e',
             'PZH03_N213_e-1',
             'PZH03_N213_f',
             'PZH03_N213_f-1',
             'PZH03_N214_a',
             'PZH03_N214_a-1',
             'PZH03_N214_b',
             'PZH03_N214_b-1',
             'PZH03_N214_c',
             'PZH03_N214_c-1',
             'PZH03_N214_d',
             'PZH03_N214_d-1',
             'PZH03_N214_e',
             'PZH03_N214_e-1',
             'PZH03_N214_f',
             'PZH03_N214_f-1',
             'PZH03_N214_g',
             'PZH03_N214_g-1',
             'PZH03_N214_h',
             'PZH03_N214_h-1',
             'PZH03_N215_a',
             'PZH03_N215_a-1',
             'PZH03_N215_b',
             'PZH03_N215_b-1',
             'PZH03_N215_c',
             'PZH03_N215_c-1',
             'PZH03_N215_d',
             'PZH03_N215_d-1',
             'PZH03_N215_e',
             'PZH03_N215_e-1',
             'PZH03_N216_a',
             'PZH03_N216_a-1',
             'PZH03_N216_b',
             'PZH03_N216_b-1',
             'PZH03_N216_c',
             'PZH03_N216_c-1',
             'PZH03_N216_d',
             'PZH03_N216_d-1',
             'PZH03_N216_e',
             'PZH03_N216_e-1',
             'PZH03_N216_f',
             'PZH03_N216_f-1',
             'PZH03_N216_g',
             'PZH03_N216_g-1',
             'PZH03_N217-1_a',
             'PZH03_N217-1_a-1',
             'PZH03_N217-1_b',
             'PZH03_N217-1_b-1',
             'PZH03_N217-1_c',
             'PZH03_N217-1_c-1',
             'PZH03_N217-1_d',
             'PZH03_N217-1_d-1',
             'PZH03_N217-1_e',
             'PZH03_N217-1_e-1',
             'PZH03_N217-1_f',
             'PZH03_N217-1_f-1',
             'PZH03_N217-2_a',
             'PZH03_N217-2_a-1',
             'PZH03_N217-2_b',
             'PZH03_N217-2_b-1',
             'PZH03_N217-2_c',
             'PZH03_N217-2_c-1',
             'PZH03_N217-2_d',
             'PZH03_N217-2_d-1',
             'PZH03_N217-2_e',
             'PZH03_N217-2_e-1',
             'PZH03_N217-2_f',
             'PZH03_N217-2_f-1',
             'PZH03_N217-2_g',
             'PZH03_N217-2_g-1',
             'PZH03_N217-2_h',
             'PZH03_N217-2_h-1',
             'PZH03_N217-2_i',
             'PZH03_N217-2_i-1',
             'PZH03_N218-1_a',
             'PZH03_N218-1_a-1',
             'PZH03_N218-1_b',
             'PZH03_N218-1_b-1',
             'PZH03_N218-1_c',
             'PZH03_N218-1_c-1',
             'PZH03_N218-1_d',
             'PZH03_N218-1_d-1',
             'PZH03_N218-1_e',
             'PZH03_N218-1_e-1',
             'PZH03_N218-1_f',
             'PZH03_N218-1_f-1',
             'PZH03_N218-1_g',
             'PZH03_N218-1_g-1',
             'PZH03_N218-1_h',
             'PZH03_N218-1_h-1',
             'PZH03_N218-1_i',
             'PZH03_N218-1_i-1',
             'PZH03_N218-2_a',
             'PZH03_N218-2_a-1',
             'PZH03_N218-2_b',
             'PZH03_N218-2_b-1',
             'PZH03_N218-2_c',
             'PZH03_N218-2_c-1',
             'PZH03_N218-2_d',
             'PZH03_N218-2_d-1',
             'PZH03_N219_a',
             'PZH03_N219_a-1',
             'PZH03_N219_b',
             'PZH03_N219_b-1',
             'PZH03_N219_c',
             'PZH03_N219_c-1',
             'PZH03_N219_d',
             'PZH03_N219_d-1',
             'PZH03_N219_e',
             'PZH03_N219_e-1',
             'PZH03_N219_f',
             'PZH03_N219_f-1',
             'PZH03_N219_g',
             'PZH03_N219_g-1',
             'PZH03_N219_h',
             'PZH03_N219_h-1',
             'PZH03_N219_i',
             'PZH03_N219_i-1',
             'PZH03_N220-1_a',
             'PZH03_N220-1_a-1',
             'PZH03_N220-1_b',
             'PZH03_N220-1_b-1',
             'PZH03_N220-1_c',
             'PZH03_N220-1_c-1',
             'PZH03_N220-1_d',
             'PZH03_N220-1_d-1',
             'PZH03_N220-2_a',
             'PZH03_N220-2_a-1',
             'PZH03_N220-2_b',
             'PZH03_N220-2_b-1',
             'PZH03_N220-2_c',
             'PZH03_N220-2_c-1',
             'PZH03_N222_a',
             'PZH03_N222_a-1',
             'PZH03_N222_b',
             'PZH03_N222_b-1',
             'PZH03_N222_c',
             'PZH03_N222_c-1',
             'PZH03_N222_d',
             'PZH03_N222_d-1',
             'PZH03_N223_a',
             'PZH03_N223_a-1',
             'PZH03_N223_b',
             'PZH03_N223_b-1',
             'PZH03_N223_c',
             'PZH03_N223_c-1',
             'PZH03_N223_d',
             'PZH03_N223_d-1',
             'PZH03_N223_e',
             'PZH03_N223_e-1',
             'PZH03_N223_f',
             'PZH03_N223_f-1',
             'PZH03_N223_g',
             'PZH03_N223_g-1',
             'PZH03_N223_h',
             'PZH03_N223_h-1',
             'PZH03_N228_a',
             'PZH03_N228_a-1',
             'PZH03_N228_b',
             'PZH03_N228_b-1',
             'PZH03_N228_c',
             'PZH03_N228_c-1',
             'PZH03_N228_d',
             'PZH03_N228_d-1',
             'PZH03_N231_a',
             'PZH03_N231_a-1',
             'PZH03_N231_b',
             'PZH03_N231_b-1',
             'PZH03_N231_c',
             'PZH03_N231_c-1',
             'PZH03_N440_a',
             'PZH03_N440_a-1',
             'PZH03_N440_b',
             'PZH03_N440_b-1',
             'PZH03_N441_a',
             'PZH03_N441_a-1',
             'PZH03_N441_b',
             'PZH03_N441_b-1',
             'PZH03_N441_c',
             'PZH03_N441_c-1',
             'PZH03_N442_a',
             'PZH03_N442_a-1',
             'PZH03_N443_a',
             'PZH03_N443_a-1',
             'PZH03_N443_b',
             'PZH03_N443_b-1',
             'PZH03_N443_c',
             'PZH03_N443_c-1',
             'PZH03_N444_a',
             'PZH03_N444_a-1',
             'PZH03_N444_b',
             'PZH03_N444_b-1',
             'PZH03_N444_c',
             'PZH03_N444_c-1',
             'PZH03_N444_d',
             'PZH03_N444_d-1',
             'PZH03_N444_e',
             'PZH03_N444_e-1',
             'PZH03_N444_f',
             'PZH03_N444_f-1',
             'PZH03_N444_g',
             'PZH03_N444_g-1',
             'PZH03_N445_a',
             'PZH03_N445_a-1',
             'PZH03_N445_b',
             'PZH03_N445_b-1',
             'PZH03_N445_c',
             'PZH03_N445_c-1',
             'PZH03_N445_d',
             'PZH03_N445_d-1',
             'PZH03_N446_a',
             'PZH03_N446_a-1',
             'PZH03_N446_b',
             'PZH03_N446_b-1',
             'PZH03_N446_c',
             'PZH03_N446_c-1',
             'PZH03_N446_d',
             'PZH03_N446_d-1',
             'PZH03_N446_e',
             'PZH03_N446_e-1',
             'PZH03_N446_f',
             'PZH03_N446_f-1',
             'PZH03_N447_a',
             'PZH03_N447_a-1',
             'PZH03_N447_b',
             'PZH03_N447_b-1',
             'PZH03_N447_c',
             'PZH03_N447_c-1',
             'PZH03_N447_d',
             'PZH03_N447_d-1',
             'PZH03_N447_e',
             'PZH03_N447_e-1',
             'PZH03_N448_a',
             'PZH03_N448_a-1',
             'PZH03_N449_a',
             'PZH03_N449_a-1',
             'PZH03_N449_b',
             'PZH03_N449_b-1',
             'PZH03_N449_c',
             'PZH03_N449_c-1',
             'PZH03_N450_a',
             'PZH03_N450_a-1',
             'PZH03_N450_b',
             'PZH03_N450_b-1',
             'PZH03_N450_c',
             'PZH03_N450_c-1',
             'PZH03_N451_a',
             'PZH03_N451_a-1',
             'PZH03_N451_b',
             'PZH03_N451_b-1',
             'PZH03_N451_c',
             'PZH03_N451_c-1',
             'PZH03_N451_d',
             'PZH03_N451_d-1',
             'PZH03_N451_e',
             'PZH03_N451_e-1',
             'PZH03_N452_a',
             'PZH03_N452_a-1',
             'PZH03_N453_a',
             'PZH03_N453_a-1',
             'PZH03_N453_b',
             'PZH03_N453_b-1',
             'PZH03_N453_c',
             'PZH03_N453_c-1',
             'PZH03_N454_a',
             'PZH03_N454_a-1',
             'PZH03_N454_b',
             'PZH03_N454_b-1',
             'PZH03_N455_a',
             'PZH03_N455_a-1',
             'PZH03_N455_b',
             'PZH03_N455_b-1',
             'PZH03_N457_a',
             'PZH03_N457_a-1',
             'PZH03_N457_b',
             'PZH03_N457_b-1',
             'PZH03_N457_c',
             'PZH03_N457_c-1',
             'PZH03_N457_d',
             'PZH03_N457_d-1',
             'PZH03_N457_e',
             'PZH03_N457_e-1',
             'PZH03_N457_f',
             'PZH03_N457_f-1',
             'PZH03_N458_a',
             'PZH03_N458_a-1',
             'PZH03_N458_b',
             'PZH03_N458_b-1',
             'PZH03_N459_a',
             'PZH03_N459_a-1',
             'PZH03_N459_b',
             'PZH03_N459_b-1',
             'PZH03_N459_c',
             'PZH03_N459_c-1',
             'PZH03_N460_a',
             'PZH03_N460_a-1',
             'PZH03_N461_a',
             'PZH03_N461_a-1',
             'PZH03_N462_a',
             'PZH03_N462_a-1',
             'PZH03_N463_a',
             'PZH03_N463_a-1',
             'PZH03_N464_a',
             'PZH03_N464_a-1',
             'PZH03_N464_b',
             'PZH03_N464_b-1',
             'PZH03_N464_c',
             'PZH03_N464_c-1',
             'PZH03_N466_Naaldwijk_a',
             'PZH03_N466_Naaldwijk_a-1',
             'PZH03_N466_Naaldwijk_b',
             'PZH03_N466_Naaldwijk_b-1',
             'PZH03_N466_Naaldwijk_c',
             'PZH03_N466_Naaldwijk_c-1',
             'PZH03_N466_Naaldwijk_d',
             'PZH03_N466_Naaldwijk_d-1',
             'PZH03_N466_Naaldwijk_e',
             'PZH03_N466_Naaldwijk_e-1',
             'PZH03_N467_Naaldwijk_a',
             'PZH03_N467_Naaldwijk_a-1',
             'PZH03_N467_Naaldwijk_b',
             'PZH03_N467_Naaldwijk_b-1',
             'PZH03_N467_Naaldwijk_c',
             'PZH03_N467_Naaldwijk_c-1',
             'PZH03_N467_Naaldwijk_d',
             'PZH03_N467_Naaldwijk_d-1',
             'PZH03_N468_a',
             'PZH03_N468_a-1',
             'PZH03_N468_b',
             'PZH03_N468_b-1',
             'PZH03_N468_c',
             'PZH03_N468_c-1',
             'PZH03_N468_d',
             'PZH03_N468_d-1',
             'PZH03_N468_e',
             'PZH03_N468_e-1',
             'PZH03_N470-1_a',
             'PZH03_N470-1_a-1',
             'PZH03_N470-1_b',
             'PZH03_N470-1_b-1',
             'PZH03_N470-1_c',
             'PZH03_N470-1_c-1',
             'PZH03_N470-1_d',
             'PZH03_N470-1_d-1',
             'PZH03_N470-1_e',
             'PZH03_N470-1_e-1',
             'PZH03_N470-1_f',
             'PZH03_N470-1_f-1',
             'PZH03_N470-1_g',
             'PZH03_N470-1_g-1',
             'PZH03_N470-1_h',
             'PZH03_N470-1_h-1',
             'PZH03_N470-1_i',
             'PZH03_N470-1_i-1',
             'PZH03_N470-2_a',
             'PZH03_N470-2_b',
             'PZH03_N470_a',
             'PZH03_N470_a-1',
             'PZH03_N470_b',
             'PZH03_N470_b-1',
             'PZH03_N470_c',
             'PZH03_N470_c-1',
             'PZH03_N470_d',
             'PZH03_N470_d-1',
             'PZH03_N470_e',
             'PZH03_N470_e-1',
             'PZH03_N470_f',
             'PZH03_N470_f-1',
             'PZH03_N470_g',
             'PZH03_N470_g-1',
             'PZH03_N470_h',
             'PZH03_N470_h-1',
             'PZH03_N471_a',
             'PZH03_N471_a-1',
             'PZH03_N471_b',
             'PZH03_N471_b-1',
             'PZH03_N471_c',
             'PZH03_N471_c-1',
             'PZH03_N471_d',
             'PZH03_N471_d-1',
             'PZH03_N472_a',
             'PZH03_N472_a-1',
             'PZH03_N472_b',
             'PZH03_N472_b-1',
             'PZH03_N472_c',
             'PZH03_N472_c-1',
             'PZH03_N474_a',
             'PZH03_N474_a-1',
             'PZH03_N475_a',
             'PZH03_N475_a-1',
             'PZH03_N476_a',
             'PZH03_N476_a-1',
             'PZH03_N477_a',
             'PZH03_N477_a-1',
             'PZH03_N478_a',
             'PZH03_N478_a-1',
             'PZH03_N479_a',
             'PZH03_N479_a-1',
             'PZH03_N480_a',
             'PZH03_N480_a-1',
             'PZH03_N480_b',
             'PZH03_N480_b-1',
             'PZH03_N481_a',
             'PZH03_N481_a-1',
             'PZH03_N482_a',
             'PZH03_N482_a-1',
             'PZH03_N484_a',
             'PZH03_N484_a-1',
             'PZH03_N487_a',
             'PZH03_N487_a-1',
             'PZH03_N488_a',
             'PZH03_N488_a-1',
             'PZH03_N488_b',
             'PZH03_N488_b-1',
             'PZH03_N488_c',
             'PZH03_N488_c-1',
             'PZH03_N489_a',
             'PZH03_N489_a-1',
             'PZH03_N489_b',
             'PZH03_N489_b-1',
             'PZH03_N489_c',
             'PZH03_N489_c-1',
             'PZH03_N491_a',
             'PZH03_N491_a-1',
             'PZH03_N492_Spijkenisse_d',
             'PZH03_N492_Spijkenisse_d-1',
             'PZH03_N492_Spijkenisse_f',
             'PZH03_N492_Spijkenisse_f-1',
             'PZH03_N492_Spijkenisse_g',
             'PZH03_N492_Spijkenisse_g-1',
             'PZH03_N492_Spijkenisse_h',
             'PZH03_N492_Spijkenisse_h-1',
             'PZH03_N492_a',
             'PZH03_N492_a-1',
             'PZH03_N492_b',
             'PZH03_N492_b-1',
             'PZH03_N492_c',
             'PZH03_N492_c-1',
             'PZH03_N492_d',
             'PZH03_N492_d-1',
             'PZH03_N492_e',
             'PZH03_N492_e-1',
             'PZH03_N492_f',
             'PZH03_N492_f-1',
             'PZH03_N492_g',
             'PZH03_N492_g-1',
             'PZH03_N492_h',
             'PZH03_N492_h-1',
             'PZH03_N493_a',
             'PZH03_N493_a-1',
             'PZH03_N493_b',
             'PZH03_N493_b-1',
             'PZH03_N494_a',
             'PZH03_N494_a-1',
             'PZH03_N495_a',
             'PZH03_N495_a-1',
             'PZH03_N496_a',
             'PZH03_N496_a-1',
             'PZH03_N496_b',
             'PZH03_N496_b-1',
             'PZH03_N497_a',
             'PZH03_N497_a-1',
             'PZH03_N498_a',
             'PZH03_N498_a-1']
    shape = gpd.read_file("../data/044.1_Levering_NDW_Shapefiles_20180925/20180925_Meetvakken.dbf")

    def add_basemap(ax, zoom, url='http://tile.stamen.com/terrain/tileZ/tileX/tileY.png'):
        """Taken from the GeoPandas documentation."""
        xmin, xmax, ymin, ymax = ax.axis()
        basemap, extent = ctx.bounds2img(xmin, ymin, xmax, ymax, zoom=zoom, url=url)
        ax.imshow(basemap, extent=extent, interpolation='bilinear')
        # restore original x/y limits
        ax.axis((xmin, xmax, ymin, ymax))

    # Convert to correct datum.
    shape = shape.to_crs(epsg=3857)

    ax = shape[shape["naam"].isin(roads)].plot()
    add_basemap(ax, zoom=9)
    plt.axis('off')
    plt.tight_layout()
    plt.savefig("../thesis/figures/roads.pdf")
