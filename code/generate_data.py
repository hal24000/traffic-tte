"""Generators and formatters for Weibull time-to-event RNN test data."""

import numpy as np


class TestData:
    """
    Generate data that can be used to test Weibull time-to-event RNNs.

    Most of the code in this class is adapted from the standalone example
    notebook by Martinnson.

    Args:
        no_of_timesteps: Lenght of each generated sequence.
        no_of_sequences: Number of sequences to generate.
        event_every_nth: An event occurs every nth timestep.
        no_of_repeats: Number of times to repeat the generated sequences in
                       the training set.
        enable_censoring: Whether or not to enable censoring when generating
                          the training data. With censoring enabled the
                          problem can be seen as us having measurements
                          until some time, with the rest of the observations
                          still to be done, while disabling censoring means
                          we have all measurements.
        noise_level: Noise level to include when generating training data.
    """

    def __init__(self, no_of_timesteps: int, no_of_sequences: int,
                 event_every_nth: int, no_of_repeats: int,
                 enable_censoring: bool, noise_level: float):
        self.no_of_timesteps = no_of_timesteps
        self.no_of_sequences = no_of_sequences
        self.event_every_nth = event_every_nth
        self.no_of_repeats = no_of_repeats
        self.enable_censoring = enable_censoring
        self.noise_level = noise_level

        self.true_tte = None
        self.tte_censored = None
        self.events = None
        self.was_event = None
        self.is_censored = None
        self.is_not_censored = None

        self.u_train = None
        self.x_train = None
        self.y_train = None

        self.u_test = None
        self.x_test = None
        self.y_test = None

    def _generate_data(self):
        """Generate the raw data for use in testing."""
        # This array will allow us to generate events by taking modulo of the
        # array.
        _raw_events = np.tile(np.array(range(self.no_of_timesteps)),
                              (self.no_of_sequences, 1))
        _raw_events += np.array(range(self.event_every_nth))[np.newaxis].T + 1

        # This is the ground truth: the actual time until an event occurs.
        self.true_tte = ((self.event_every_nth - 1 - _raw_events) %
                         self.event_every_nth)

        # The actual points in time at which an event occurs.
        # These are the times where time-to-event is 0.
        self.events = (self.true_tte == 0) * 1.0

        # An indicator, equal to 1 if the last step had an event occurring,
        # 0 otherwise.
        _was_event = ((_raw_events % self.event_every_nth) == 0) * 1.0
        # There is never an event before the start of a sequence, so set the
        # first elements to 0.
        _was_event[:, 0] = 0.0
        self.was_event = _was_event

        # Whether the next event is actually in the data.
        # If so, indicator is 0, otherwise it is 1.
        self.is_censored = (self.events[:, ::-1].cumsum(1)[:, ::-1] == 0) * 1.0

        # The complement of is_censored.
        self.is_not_censored = 1 - self.is_censored

        # Generate a mask for those timesteps that are censored, and get the
        # time until the end of the measurement window.
        _tte_censored = (self.is_censored[:, ::-1].cumsum(1)[:, ::-1] *
                         self.is_censored)

        # Add time-to-event for uncensored timesteps to the censored timesteps'
        # time-to-event.
        self.tte_censored = (_tte_censored + (1 - self.is_censored) *
                             self.true_tte)

    def format_data(self):
        """Format the data correctly for use in RNNs."""
        self._generate_data()

        # Training censoring indicator.
        self.u_train = self.is_not_censored.reshape(self.no_of_sequences,
                                                    self.no_of_timesteps, 1)

        # We use was_event for training due to the nature of RNNs.
        # The output of an RNN is always lagged one step with respect to input.
        _x_train = self.was_event.reshape(self.no_of_sequences,
                                          self.no_of_timesteps, 1)
        _y_train = np.append(
            self.tte_censored.reshape(self.no_of_sequences,
                                      self.no_of_timesteps, 1),
            self.u_train, axis=2)

        # Test censoring indicator. Since the test data is fully known, all data
        # is uncensored.
        self.u_test = np.ones(shape=(self.no_of_sequences, self.no_of_timesteps,
                                     1))
        self.x_test = np.copy(_x_train)
        self.y_test = np.append(
            self.true_tte.reshape(self.no_of_sequences, self.no_of_timesteps,
                                  1),
            self.u_test, axis=2)

        # If censoring is not enabled, the training set is the same as the test
        # set, since we know when all events occur.
        if not self.enable_censoring:
            _y_train = np.copy(self.y_test)

        # Now we extend the training set with repetitions of the previously
        # generated data.
        _tiled_x_train = np.tile(_x_train, (self.no_of_repeats, 1, 1))
        self.y_train = np.tile(_y_train, (self.no_of_repeats, 1, 1))

        # Since the data is at present deterministic, we add some noise since
        # that is also more realistic when considering real world situations.
        _noise = np.random.binomial(1, self.noise_level,
                                    size=_tiled_x_train.shape)
        # Noise is XORed with the extended training set.
        self.x_train = _tiled_x_train + _noise - _tiled_x_train * _noise
