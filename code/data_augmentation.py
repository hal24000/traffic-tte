import itertools

import matplotlib.pyplot as plt

import numpy as np

import pandas as pd

from data_loading import drop_columns, load_FCD
from plotting import plot_over_day


def load_and_augment_data(path, min_non_jam_length, min_jam_length, jam_ratio, show_final_statistics=False, show_plots=True):
    """
    min_non_jam_length: gaps between jams shorter than this length are discarded (merged with surrounding jams)
    min_jam_length: jams shorter than this length are discarded
    jam_ratio: ratio below which a jam is considered to have occured
    """
    print("Loading data.")
    df = load_FCD(path)
    df = calculate_speeds(df)
    df = annotate_jams(df, jam_ratio=jam_ratio, show_plots=show_plots)

    df = merge_jams(df, min_non_jam_length, min_jam_length, show_final_statistics, show_plots=show_plots)

    drop_columns(df, columns_to_drop=["maxLocalJamLength", "maxLocalNonJamLength"])

    print("Calculating time-to-events.")
    df = calculate_ttes(df, is_jam_column="isJamCleaned")

    df = add_weekday(df)

    return df


def add_weekday(df):
    df["dayOfWeek"] = df["periodStart"].dt.weekday
    return df


def remove_consecutive_counts(df, column):
    """Remove consecutive same values, since these correspond to a single event."""
    lengths = df.groupby("measurementSiteReference")[column].apply(lambda x: x.to_list())
    # https://stackoverflow.com/a/5738933
    cleaned_lengths = list(itertools.chain(*[[x[0] for x in itertools.groupby(k)] for k in lengths]))
    return cleaned_lengths


def merge_jams(df, min_non_jam_length, min_jam_length, show_final_statistics=False, show_plots=True):
    """
    show_final_statistics: if True, recalculate jam and non-jam lengths again after merging and discarding and show plots to make sure merging and discarding worked correctly.
    """
    print("Calculating jam lengths.")
    df = get_jam_lengths(df)

    unique, counts = np.unique(remove_consecutive_counts(df, "maxLocalNonJamLength"), return_counts=True)
    # Discard the first value, since it corresponds to all jam moments.
    print(f"Most common non-jam length: {unique[np.argmax(counts[1:]) + 1]} minutes")
    if show_plots:
        plt.plot(unique[1:1000], counts[1:1000], "bo")
        plt.xlabel("Length of non-jams (minutes)")
        plt.ylabel("Number of occurences")
        plt.xlim(0, 100)
        plt.show()

    unique, counts = np.unique(remove_consecutive_counts(df, "maxLocalJamLength"), return_counts=True)
    # Discard the first value, since it corresponds to all non-jam moments.
    print(f"Most common jam length: {unique[np.argmax(counts[1:]) + 1]} minutes")
    if show_plots:
        plt.plot(unique[1:1000], counts[1:1000], "bo")
        plt.xlabel("Length of jams (minutes)")
        plt.ylabel("Number of occurences")
        plt.xlim(0, 100)
        plt.show()

    print("Number of jams before merging:")
    print(df["isJam"].value_counts())

    # Merge jams that are close together.
    print("Merging close jams.")
    df["isJamCleaned"] = df["isJam"]
    df.loc[df["maxLocalNonJamLength"] < min_non_jam_length, "isJamCleaned"] = 1

    print("Number of jams after merging:")
    print(df["isJamCleaned"].value_counts())

    # Recalculate jam lengths, for filtering of short jams.
    print("Recalculating jam lengths.")
    df = get_jam_lengths(df, is_jam_column="isJamCleaned")

    unique, counts = np.unique(remove_consecutive_counts(df, "maxLocalNonJamLength"), return_counts=True)
    # Discard the first value, since it corresponds to all jam moments.
    print(f"Most common non-jam length: {unique[np.argmax(counts[1:]) + 1]} minutes")
    if show_plots:
        plt.plot(unique[1:1000], counts[1:1000], "bo")
        plt.xlabel("Length of non-jams (minutes)")
        plt.ylabel("Number of occurences")
        plt.xlim(0, 100)
        plt.show()

    unique, counts = np.unique(remove_consecutive_counts(df, "maxLocalJamLength"), return_counts=True)
    # Discard the first value, since it corresponds to all the non-jam moments.
    print(f"Most common jam length: {unique[np.argmax(counts[1:]) + 1]} minutes")
    if show_plots:
        plt.plot(unique[1:1000], counts[1:1000], "bo")
        plt.xlabel("Length of jams (minutes)")
        plt.ylabel("Number of occurences")
        plt.xlim(0, 100)
        plt.show()

    print("Number of jams before discarding:")
    print(df["isJamCleaned"].value_counts())

    # Discard short jams.
    print("Discarding short jams.")
    df.loc[df["maxLocalJamLength"] < min_jam_length, "isJamCleaned"] = 0

    print("Number of jams after discarding:")
    print(df["isJamCleaned"].value_counts())

    if show_final_statistics:
        # Recalculate jam lengths, for statistics only.
        print("Recalculating jam lengths (for statistics).")
        df = get_jam_lengths(df, is_jam_column="isJamCleaned")

        unique, counts = np.unique(remove_consecutive_counts(df, "maxLocalNonJamLength"), return_counts=True)
        # Discard the first value, since it corresponds to all jam moments.
        print(f"Most common non-jam length: {unique[np.argmax(counts[1:]) + 1]} minutes")
        plt.plot(unique[1:1000], counts[1:1000], "bo")
        plt.xlabel("Length of non-jams (minutes)")
        plt.ylabel("Number of occurences")
        plt.xlim(0, 100)
        plt.show()

        unique, counts = np.unique(remove_consecutive_counts(df, "maxLocalJamLength"), return_counts=True)
        # Discard the first value, since it corresponds to all the non-jam moments.
        print(f"Most common jam length: {unique[np.argmax(counts[1:]) + 1]} minutes")
        plt.plot(unique[1:1000], counts[1:1000], "bo")
        plt.xlabel("Length of jams (minutes)")
        plt.ylabel("Number of occurences")
        plt.xlim(0, 100)
        plt.show()

    return df


def clip_tte(df, clip_length, use_log=False, show_plots=False):
    """
    use_log: if True, ignore the clip_length and instead use log(x + 1) as transform
    """
    if use_log:
        df["timeToJamClipped"] = np.log1p(df["timeToJam"])
        df["timeToUnjamClipped"] = np.log1p(df["timeToUnjam"])
    else:
        df["timeToJamClipped"] = np.clip(df["timeToJam"], a_min=0, a_max=clip_length)
        df["timeToUnjamClipped"] = np.clip(df["timeToUnjam"], a_min=0, a_max=clip_length)

    if show_plots:
        print("Distribution of time-to-event for jams.")
        plt.hist(df["timeToJamClipped"].to_numpy(), bins=range(0, clip_length+2), align="left")
        plt.title("TTE jams (all)")
        plt.xlabel("number of occurences")
        plt.ylabel("time-to-event")
        plt.show()
        values = df["timeToJamClipped"].to_numpy()
        plt.hist(values[(values > 0) & (values < clip_length)], bins=range(1, clip_length+1), align="left")
        plt.title(f"TTE jams (zoomed to interval [1, {clip_length-1}])")
        plt.xlabel("number of occurences")
        plt.ylabel("time-to-event")
        plt.show()

        print("Distribution of time-to-event for unjams.")
        plt.hist(df["timeToUnjamClipped"].to_numpy(), bins=range(0, clip_length+2), align="left")
        plt.title("TTE unjams (all)")
        plt.xlabel("number of occurences")
        plt.ylabel("time-to-event")
        plt.show()
        values = df["timeToUnjamClipped"].to_numpy()
        plt.hist(values[(values > 0) & (values < clip_length)], bins=range(1, clip_length+1), align="left")
        plt.title(f"TTE unjams (zoomed to interval [1, {clip_length-1}])")
        plt.xlabel("number of occurences")
        plt.ylabel("time-to-event")
        plt.show()

    return df


def calculate_speeds(df):
    print("NaNs before:")
    print(df.isna().sum())

    # Replace missing values by forward filling for that particular road.
    # Also fill the first few observations by backfulling if they are NaNs.
    # Note that it is important to first group by road segment, since otherwise filling will happen consecutively, instead of per road.
    df["avgTravelTime"] = df.groupby("measurementSiteReference")["avgTravelTime"].ffill().bfill()

    # Remove NaNs in "referenceAvgTravelTime".
    df["referenceAvgTravelTime"] = df.groupby("measurementSiteReference")["referenceAvgTravelTime"].ffill().bfill()

    # Calculate reference speeds.
    df["referenceAvgVehicleSpeed"] = df["lengthAffected"] / df["referenceAvgTravelTime"]

    # Calculate vehicle speeds.
    df["avgVehicleSpeed"] = df["lengthAffected"] / df["avgTravelTime"]

    # Clip unreasonable speeds (larger than 130 kph).
    df["avgVehicleSpeed"] = np.clip(df["avgVehicleSpeed"], a_min=0, a_max=130/3.6)

    df["ratioReference"] = df["avgVehicleSpeed"] / df["referenceAvgVehicleSpeed"]


    print("\nNaNs after:")
    print(df.isna().sum())

    drop_columns(df, ["referenceAvgVehicleSpeed", "avgTravelTime", "referenceAvgTravelTime", "lengthAffected"])

    return df


def calculate_ttes(df, is_jam_column):
    # Initialize latent target variable columns.
    df["timeToJam"] = 0
    df["timeToJamUncensored"] = 0
    df["timeToUnjam"] = 0
    df["timeToUnjamUncensored"] = 0

    df["isNotJam"] = (~df[is_jam_column].astype(bool)).astype(int)

    # Pandas magic to set the censoring indicator. Doing this as a for-loop (along with the rest of the latent variables) takes more than 1 hour.
    # We apply a reverse cumulative sum, and set the uncensoring indicator to 0 if there is no event.
    df["timeToJamUncensored"] = 1 - (df.groupby("measurementSiteReference")[is_jam_column].apply(lambda x: x[::-1].cumsum()) == 0)[::-1].astype(int).reset_index(level=0, drop=True)

    # https://stackoverflow.com/a/45965003
    df["timeToJam"] = df.groupby("measurementSiteReference")["isNotJam"].apply(lambda x: x[::-1].cumsum() - x[::-1].cumsum().where(~x.astype(bool)).ffill().fillna(0))[::-1].astype(int).reset_index(level=0, drop=True)

    df["timeToUnjamUncensored"] = 1 - (df.groupby("measurementSiteReference")["isNotJam"].apply(lambda x: x[::-1].cumsum()) == 0)[::-1].astype(int).reset_index(level=0, drop=True)

    df["timeToUnjam"] = df.groupby("measurementSiteReference")[is_jam_column].apply(lambda x: x[::-1].cumsum() - x[::-1].cumsum().where(~x.astype(bool)).ffill().fillna(0))[::-1].astype(int).reset_index(level=0, drop=True)

    drop_columns(df, ["isNotJam"])

    return df


def annotate_jams(df, jam_ratio, show_statistics=True, show_plots=True):
    df["isJam"] = (df["ratioReference"] < jam_ratio).astype(int)

    if show_statistics:
        if show_plots:
            plot_over_day([df[df["isJam"] == 1].groupby([df.periodStart.dt.hour, df.periodStart.dt.minute])["isJam"].value_counts().to_list()], labels=["jam"])

        print("Number of jams (1 = jam)")
        print(df["isJam"].value_counts())

    return df


def get_jam_lengths(df, is_jam_column="isJam"):
    # Complement of isJams, useful for calculating the length of non-jammed intervals
    df["isNotJam"] = (~df[is_jam_column].astype(bool)).astype(int)

    # Cumulative length of traffic jams, resetting after every jam.
    df["cumulativeLocalJamLength"] = df.groupby("measurementSiteReference")[is_jam_column].apply(lambda x: x.cumsum() - x.cumsum().where(~x.astype(bool)).ffill().fillna(0)).astype(int).reset_index(level=0, drop=True)


    # Cumulative length of non-jams, resetting after every non-jam.
    df["cumulativeLocalNonJamLength"] = df.groupby("measurementSiteReference")["isNotJam"].apply(lambda x: x.cumsum() - x.cumsum().where(~x.astype(bool)).ffill().fillna(0)).astype(int).reset_index(level=0, drop=True)

    # Replace 0 by NaNs, to make use of the fact that cummax() skips NaNs.
    df["maxLocalJamLength"] = df["cumulativeLocalJamLength"].replace(0, np.nan)

    # Calculate the maximum length of a jam, and place that value over the entire length of the jam.
    # This is useful for being able to filter out short jams.
    # https://stackoverflow.com/a/52650141
    df["maxLocalJamLength"] = df.groupby("measurementSiteReference").apply(lambda x: x["maxLocalJamLength"][::-1].groupby(x["isNotJam"][::-1].cumsum()).cummax())[::-1].replace(np.nan, 0).astype(int).reset_index(level=0, drop=True)

    # Replace 0 by NaNs, to make use of the fact that cummax() skips NaNs.
    df["maxLocalNonJamLength"] = df["cumulativeLocalNonJamLength"].replace(0, np.nan)

    # Calculate the maximum length of a non-jam, and place that value over the entire length of the non-jam.
    # https://stackoverflow.com/a/52650141
    df["maxLocalNonJamLength"] = df.groupby("measurementSiteReference").apply(lambda x: x["maxLocalNonJamLength"][::-1].groupby(x[is_jam_column][::-1].cumsum()).cummax())[::-1].replace(np.nan, 0).astype(int).reset_index(level=0, drop=True)

    drop_columns(df, ["isNotJam", "cumulativeLocalJamLength", "cumulativeLocalNonJamLength"])

    return df
