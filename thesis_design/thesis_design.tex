% \documentclass[a4paper,pdf]{article} % gebruik acm style voor je scriptie: [sigconf, format=acmsmall, screen=true, review=false]{acmart} 
\documentclass[sigconf, format=acmsmall, screen=true, review=false, 9pt]{acmart}
% \documentclass[sigconf]{acmart}

 
\usepackage{hyperref}
\usepackage{pdfpages} % http://mirror.unl.edu/ctan/macros/latex/contrib/pdfpages/pdfpages.pdf
\usepackage{booktabs} 

\usepackage{lineno}

\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{listings}
\usepackage{pdfpages}
\usepackage{tcolorbox}
\usepackage{float}
\usepackage{caption}
\usepackage{fancyhdr}
\usepackage{subcaption}
\usepackage{doi}
\usepackage{varioref} % Automatically put reference to page in reference. Use \vref.
\usepackage{cleveref} % Automate "equation (...)" reference. Use \vref.


% https://tex.stackexchange.com/a/346309
\settopmatter{printacmref=false}
\setcopyright{none}
\renewcommand\footnotetextcopyrightpermission[1]{}
\pagestyle{plain} % removes running headers


\makeatletter
\let\@authorsaddresses\@empty
\makeatother


\fancyfoot{}

% when writing in Dutch
%\usepackage[dutch]{babel}
%\selectlanguage{dutch}

% \linenumbers  


\begin{document}


\title{Predicting traffic jams with time-to-event RNN}
\author{Performed by Teun Zwart}
\author{Supervised by Dr. Samuel Blake}
\affiliation{\institution{HAL24K}}


\begin{abstract}
Traffic jams are costly and time consuming, and clearing them up quickly is an important task on Dutch roads.
This can be done more efficiently if traffic jams can be predicted ahead of time.
To that end we will use the WTTE-RNN (Weibull time-to-event recurrent neural network), proposed in~\cite{martinsson}, to predict parameters of the Weibull distribution to gain a predicted distribution for the time until a traffic jam occurs.
These predictions will be done using a RNN, which takes as input traffic speeds on roads.

Since the available speed data has not been labelled to indicate traffic jams, an automated way to extract such features will also be explored in this project.
Here we will consider both methods to classify a single time series, as well as multiple (possibly covering multiple roads).

Since the available speed data has a high number of invalid measurements, special attention will have to be paid to making the method robust against missing data.
\end{abstract}


\maketitle
\thispagestyle{empty}

%\todototoc
%\listoftodos
\tableofcontents


\section{Personal details}

\begin{description}
 \item[Email] \url{mailto:teunzwart@gmail.com}
 \item[Supervisor email] \url{mailto: samuel.blake@hal24k.com}
 \item[GitHub repository] \url{https://github.com/teunzwart/traffic-tte}
 \end{description} 

\section{Research question}

%A clearly defined research problem and corresponding subquestions **(20)**
%	* Can the problem be answered?
%	* Do answers to the subquestions indeed help in an understanding of the research problem or even in solving the research problem?
%	* Are the subquestions detailed enough?

Due to economic growth the number of vehicles on Dutch road is increasing, along with the associated number of traffic jams.
In 2018, the number of traffic jams and the associated traffic delay increased by 20\% compared to 2017~\cite{filezwaarte}.
These delays cost money and time, and it is therefore important that road managers (such as Rijkswaterstaat\footnote{Rijkswaterstaat is the government agency responsible for Dutch national road and waterways.}) are able to resolve such delays quickly.

At present Rijkswaterstaat does this by positioning road inspectors (who are responsible for ensuring traffic flow) at strategic positions during rush hours~\cite{beteredoorstroming}.
Road inspectors are the first responders for traffic disruptions, and are tasked with clearing the road as quickly as possible. 

In order for this to be possible, it is important that road inspectors are quickly aware of where a traffic accident or other disruption has occured.
In this thesis we will work on a method to predict whether a slowdown will occur, based on the speed of traffic over the preceding time period.

During this research, we will focus on the following questions:
\begin{enumerate}
\item Given a time series of speed data on a road, how accurately can we predict when traffic jams occur?
\item (Corollary to 2) How accurately can we predict the resumption of normal traffic conditions during a traffic jam?
\item How do we extract traffic jams from unlabelled speed time series?
\item How long does it currently take before road managers are aware a traffic jam has occured?
\end{enumerate}

Let us look at these questions in more detail.
The first question is the main point of focus in this thesis, around which all other actions revolve.
The ultimate goal is to be able to accurately predict when slowdowns occur.

The second question is then a natural extension of the first one, since once a slowdown occurs, an accurate estimate for the time needed to resolve the issue will aid in improving traffic flow.
The techniques used in the first question should naturally extend to this situation.

The third question is important because the data is unlabeled.
We therefore need a way to extract traffic jams from the data automatically.
We can cheat somewhat by using the definition of traffic jams used by Rijkswaterstaat (see section~\ref{sec:definition}), but this definition is inflexible.
Most notably, it does not classify slowdowns as traffic jams unless the speed of traffic dips below 50 km/h, while 60 km/h on a 130 km/h road may also count as a disruption.

The fourth question will be useful when performing validation, as it allows us to compare the results of the method with current best practices.
An online search did not yield answers to this question, so we will contact the VID\footnote{Verkeersinformatiedienst, a sub directorate of Rijkswaterstaat tasked with providing traffic information.} for more information.\footnote{The VID has an information phone number: 0800-8002 (open 7 days a week, 7:00-20:00 on working days, 10:00-18:30 on other days).}


\section{Related Literature}\

% Overview of the state of the art of the literature **(20)**
%	* One expects that the research problem is grounded in the literature and that each subquestion or field has a small section of relevant literature.
%	* All parts of the thesis should be grounded in or at least connected to  the literature.

Predicting events is a rich field of study.
Often called survival analysis or time-to-event estimation, such predictions range from determining when patients will die given their medical history~\cite{lee2010}, to predicting when a customer stops buying products (churn prediction)~\cite{Neslin2006}.
Note that most of these approaches focus on predicting the time between events, while we will focus predicting the time until an event occurs.

With regard to detecting outliers from time series data, failure analysis has been used for predictive maintenance, in order to detect whether an aircraft is close to failure~\cite{Korvesis2018}, as well as predicting whether a web server is about to require maintenance in~\cite{Zhang2016}, who use a recurrent neural network to predict failure probability.
These methods use time series from sensors embedded in the machine under consideration.

More specific to the problem at hand, the type of data used in this project (floating car data, see section~\ref{sec:data}) has already been used to reconstruct traffic state and density~\cite{Sunderrajan2016} and in predicting travel times~\cite{Fabri2008,Jones2013}. Notably,~\cite{Fabri2008} uses a feedforward neural network when predicting travel times.
In~\cite{Jones2013} a method of dealing with sparse FCD input is also discussed. This is interesting for this project as well, due to the amount of missing data (see section~\ref{sec:data} and section~\ref{sec:risks}). The main idea is to use the data from geographically nearby roads as a proxy for data on a road with missing data, under the assumption that traffic on nearby roads has a similar state as the road under consideration (congested or non-congested).



\section{Methodology}
%Methodology **(20)**
%	5. Do I get a clear picture of the used resources?
%		5. E.g., for data, do I get a clear picture of the data, its state, its availability, how much it is, how dirty, how much work to process, etc, etc.
%	6. Are the methods which will be used described in enough detail, so that I can picture what will be done exactly? 
%	7. Is the evaluation appropriate? That is, do I understand how each subquestion is answered by the evaluation? 

\subsection{Data}\label{sec:data}

The data used in this project will be a set of time series of speeds on Dutch roads from the NDW (National Data Warehouse for Traffic information).\footnote{Available at \url{https://dexter.ndwcloud.nu/}.}
Since credentials are necessary for this database that are not presently in the possession of the author,\footnote{An application to the portal containing the data has been made, but this platform is experiencing some technical difficulties. Even if these are not fully resolved at the start of the project, HAL24K does have a smaller subset available on which to train.} we consider a representative subset describing traffic speeds in the province of South Holland on highways and provincial roads for the period from 2017-07-01, 00:00, to 2017-07-24, 00:00, at a one minute resolution.\footnote{In total, data for a period of approximately 10 years for the entirety of the Netherlands is available.}

Each entry contains a reference to the road (segment) covered, the average travel time (in seconds), the length of the road segment (in meters), a reference travel time (in seconds), as well as an indicator for data errors (i.e. no data). From this data average vehicle speed (in m/s) is readily calculated. Other columns contain metadata about the measurements, but tend to have only one or two values (signifying the sensor type used for example), and will therefore be ignored.
A full description of all data fields is available in~\cite{datex}.
Roads may be covered by multiple measurement segments, and measurements for both the left and right side of the road are available.

The data has been collected using floating car data (FCD). FCD is collected with vehicles which determine their location using GPS. 
This allows for calculating the travel time through a specific road segment, which, combined with the length of the segment, allows for the calculation of vehicle speeds. 

An important feature to note is that the data set has many missing values.
Of 22558642 entries, only 5721179 had valid travel time measurements ($\sim$25\%).
Note however that this missing data is partly attributable to the way it has been collected.
FCD measurements only occur when enough cars with the appropriate equipment are driving on a road.
Therefore missing data entries could be better characterized as there being no measurements, as opposed to invalid measurements.
Dealing with such missing data will be an important part of the project (see section~\ref{sec:risks}).

Nevertheless, when plotting average vehicle speed over time for a road segment (\cref{fig:average_speed}, omitting invalid measurements), we see disruptions occurring as dots below a solid band (which centers around the speed limit on this road: 70 km/h or 19.4 m/s).

\begin{figure}[H]
  \centering
  \includegraphics[width=8cm]{average_speed.pdf}
  \caption{Average vehicle speed on a part of the N440 provincial road in South Holland, between 2017-07-01 and 2017-07-24. Entries without valid measurements have been omitted. The speed limit on this road is 70 km/h (19.4 m/s), which is clearly visible as the solid band of measurements. Each dot represent data for a one minute interval.}
  \label{fig:average_speed}
\end{figure}




\subsection{WTTE-RNN}

In order to predict the time until a traffic jam occurs, we will use the method proposed in~\cite{martinsson}, called Weibull time-to-event RNN (WTTE-RNN).
The idea is that, given a time series (of potentially arbitrary sensor data), with events labelled, we predict the parameters of a statistical distribution, which then gives a distribution for the time until the next event.
This method is capable of dealing with censored data (we know that an event will occur, but not when) and recurrent events, both of which occur in predicting traffic jams.

The distribution used in~\cite{martinsson} is the Weibull distribution:
\begin{align}
  \label{eq:1}
  f(t) = \frac{\beta}{\alpha} \left(\frac{t}{\alpha}\right)^{\beta-1} e^{-\left(\frac{t}{\alpha}\right)^{\beta}},
\end{align}
with \(t\in [0,\infty)\), scale parameter \(\alpha\in [0,\infty)\), and shape parameter \(\beta\in [0,\infty)\).
The Weibull distribution has a number of advantages. Most of its statistical properties have a closed form expression, it is unimodal (has a single peak), while still being expressive, and it is easily discretized.

We are interested in predicting \(\alpha\) and \(\beta\) given a time series, prompting the use of a recurrent neural network (RNN), which will also allow for processing of data with varying lengths (useful when dealing with missing data, as proposed in section~\ref{sec:risks}).
The model has to predict the time-to-event as accurately as possible, prompting the use of the error function
\begin{align}
  \label{eq:2}
  E=(y_t-\hat{y}_t)^2,
\end{align}
where \(y_t\) is the actual time at which an event occurs, and \(\hat{y}_t\) is the predicted time.
This will be the main way in which to validate the model.
Note that, since we are using a probabilistic prediction of \(\hat{y}_t\), it is more accurate to say that we use the expectation value of the distribution over time-to-event in calculating the error (the exact mappings between distributions and point estimates is elaborated on in~\cite{martinsson}).

While validating the method, it will also be important to consider whether the model generalizes to different parts of the Netherlands (traffic patterns between the urbanized western part of the country and the more rural eastern part may vary).
Validation may also be aided by an internal database of traffic incidents, as this provides a ground truth to work with.

\subsection{Defining a traffic jams}\label{sec:definition}

In order to use the above method we need a target variable describing whether a disruption of traffic is occurring.
In other words, we need a definition of a traffic jam, so we can label the data.

According to Rijkswaterstaat~\cite{definitionjam}, traffic jam\footnote{In Dutch, \textit{``file''}.} is an umbrella term for three kinds of disruptions:
\begin{itemize}
  \item \textbf{slow traffic:} traffic which, over a distance of at least 2 kilometers, does not drive faster that 50 km/h, but usually drives faster than 25 km/h
    \item \textbf{stationary traffic:} traffic driving slower than 25 km/h, over a distance of at least 2 kilometers
    \item \textbf{slow to stationary traffic:} slow traffic over a longer stretch of road, containing subsections of stationary traffic
\end{itemize}

While this is the official Dutch government definition of a traffic jam, it is somewhat restrictive (although still a good starting point when initially implementing the WTTE-RNN).
This definition does not qualify traffic travelling 60 km/h on a 130 km/h road as a disruption.
Therefore a more flexible definition is necessary.\footnote{Note that, when predicting traffic jams, we also have to keep in mind that there are two main causes: disruption because of accidents, which temporarily lower road capacity, and disruption caused by a surplus of traffic, usually during rush hour~\cite{definitionjam}.}

A way of detecting traffic jams is through the use of time series classification, where we classify a (sub-) time series as either containing a traffic jam or not.
Determining the exact algorithm to use will be part of the project, since there are both algorithms that compare the entire time series, as well as algorithms operating on subsets~\cite{Bagnall2016}


If we are interested in classifying multiple time series at the same time, such as when considering different segments of the same road (or other roads geographically close), a possible solution proposed in~\cite{recurrentauto} uses an autoencoder to downsample this \(N\)-dimensional data to two dimensions, and then perform clustering to detect outliers. 
At that point supervised classification can be used to determine which clusters represent disruptions in traffic flow.

When doing such time series classification, it is important to use the government definition as a sanity check.


\section{Risk assessment}\label{sec:risks}
%Risk assessment **(10)**
%	* Is it complete? Is is realistic? Is the backup plan executable?
%   * Describe the risks, and describe your backup plan for each of them.	

The WTTE-RNN implementation should pose the least risk since a complete description of the method used is available~\cite{martinsson}.
Furthermore, reference implementations are available.\footnote{Available at \url{https://github.com/ragulpr/wtte-rnn} (written by the author of~\cite{martinsson}) and \url{https://github.com/gm-spacagna/deep-ttf} (focusing on time-to-failure instead of time-to-event).}

The part with a larger potential for failure will be the detection of traffic disruptions from the data.
If we can not use clustering of time series data downsampled with an auto-encoder to detect disruptions, the simplest solution would be to use either the definition used by Rijkswaterstaat, or to say that traffic jams occur if speed drops below some fraction of the average or reference speed.

Another potential pitfall is missing data.
From the exploratory data analysis it is clear that there are stretches of time for which no speed data is available.
During the project we will have to experiment with different techniques of dealing with this, while also taking into account that missing data often means there was no measurement at that point in time, as opposed to an invalid measurement being done.
If the speed is only missing for a one or two minute window, a viable technique may be to use the last known good value for the missing time steps.
For longer stretches this may not suffice.
If this is the case, we may look into whether the RNN can be made robust against missing data (perhaps by decreasing confidence of predictions when input includes missing values), or whether we can partition the data so only subsets of the time series, without missing data, are used.

Finally, it should be noted that the online portal from which the data is downloaded is experiencing some technical difficulties.
If these persist, and data is not (immediately) available, a smaller data set (larger than the one considered in the exploratory data analysis) is available internally at HAL24K.



\section{Project plan}
%Project plan  **(20)**
%	* Is it complete? (I.e., every part of the work covered.)
%	* Is it realistic?
%	* Does it give a clear picture of what will be done when? 
%	* Is it possible to evaluate whether the student is on schedule at any point in time?

% You describe what you have achieved when.
% Entries refer always back to your subquestions, methodology, literature.
% You describe concrete achievements, not actions. (e.g., instead of data preparation you write all data in XXX format, well-described, ready for analysis using YYY)


\begin{table}[h!]
\small
\begin{tabular}{l|p{1.3cm}|p{6.5cm}|p{3.5cm}}
Week & Week starting & Achievement (at end of week) & Notes\\\hline
1 & April 1 & Explored related literature,  started getting a full understanding of WTTE-RNN method described in~\cite{martinsson}, communicated with VID to understand current practice in disruption detection & \\\hline
2 & April 8 & Developed a full understanding of the method and a plan on how to implement & \\\hline
3 & April 15 & Started implementing WTTE-RNN for generated data & \\\hline
4 & April 22 & Finished implementing WTTE-RNN for generated data & Hand in of related work section and logbook \\\hline
5 & April 29 & Explored ways of dealing with missing data, and transformed data set accordingly (possibly masking missing entries or segmentation of good subsets) & \\\hline
6 & May 6 & Modified WTTE-RNN implementation to work with speed data, using government definition for traffic jams & Hand in of mid-term results (go/no-go point) \\\hline
7 & May 13 & Explored ways to automatically extract traffic jams, notably~\cite{recurrentauto,Bagnall2016} &  \\\hline
8 & May 20 & Implemented traffic jam clustering and annotated data with traffic disruption events & Student finds second reader\\\hline
9 & May 27 & Validated method when trained with government definition of traffic jam & \\\hline
10 & June 3 & Validated method when trained with data containing automatically detected and tagged traffic jams, and compared with current VID performance & \\\hline
11 & June 10 & Finished thesis (partly written during project)& \\\hline
12 & June 17 & Prepared presentation & Thesis to be handed in on June~17\\\hline
13 & June 24 & Gave presentation, finished final odds and ends & Thesis defence on June~25\\
\end{tabular}
\end{table}
 

\bibliographystyle{plain}
\bibliography{thesis_design}

 
% your refs



 
\end{document}
